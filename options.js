// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
    // Use default value color = 'red' and likesColor = true.
    accounts = JSON.parse(window.localStorage.getItem('SC_users'));
    if (typeof accounts !== 'undefined' && accounts !== null){
        var ul = document.getElementById("user_list");
        for(i=0;i<accounts.length; i++){
            const $newStudent = $("<li>"+accounts[i].email+"</li>");
            $('#user_list').append($newStudent);
        }
    }
    DiscountCheckBox.checked = JSON.parse(window.localStorage.getItem('calc_discount'));
    DownloadCheckBox.checked = JSON.parse(window.localStorage.getItem('check_download'));
}

function add_account(){
    accounts = accounts || [];
    var new_user_email = document.getElementById("email").value;
    var new_user_password = document.getElementById("password").value;
    if(new_user_email !== "" && new_user_password !== ""){
        accounts.push({ email: new_user_email, password: new_user_password });
        var ul = document.getElementById("user_list");
        const $newStudent = $("<li>"+new_user_email+"</li>");
        $('#user_list').append($newStudent);
        window.localStorage.setItem('SC_users', JSON.stringify(accounts));
        //chrome.runtime.sendMessage({greeting: 'AddUser'}, function(response) {});
        //window.postMessage(JSON.stringify(accounts), window.location.origin);
    }
    document.getElementById("email").value = "";
    document.getElementById("password").value = "";
}

function delete_account(element){
    accounts = JSON.parse(window.localStorage.getItem('SC_users'));
    if (typeof accounts !== 'undefined' && accounts !== null){
        for(i=0;i<accounts.length; i++){
            if(accounts[i].email === element.target.innerText){
                //
                if(confirm("Delete "+element.target.innerText+" ?")){
                    accounts.splice(i,1);
                    window.localStorage.setItem('SC_users', JSON.stringify(accounts));
                }
            }
        }
    }
}

function update_discount(){
    window.localStorage.setItem('calc_discount',DiscountCheckBox.checked);
}

function update_download(){
    window.localStorage.setItem('check_download',DownloadCheckBox.checked);
}

var accounts = [];
var DiscountCheckBox = document.getElementById("discount_checkbox");
var DownloadCheckBox = document.getElementById("download_checkbox");
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('add').addEventListener('click',add_account);
document.getElementById("user_list").addEventListener('click', delete_account);
DiscountCheckBox.addEventListener('change',update_discount);
DownloadCheckBox.addEventListener('change',update_download);
