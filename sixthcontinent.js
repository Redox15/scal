//console.log('I SHOULD CLICK HERE');
var didit=false;
var trytwice=0;
var tryLogin=0;
var calc_Done=false;
var StartTimeOut;
var CalcTimeOut;
var BadgeTimeOut;
var SecondClickTimeOut;
var LogOutTimeOut;
var lastID=-1;
var accounts_state = [];
var LoginBadge = false;
var dateObj;
var prefix='';

function is_S_W_L_open(){
  for(i=0;i<$('.drop-content').length;i++){
    if($('.drop-content')[i].scrollWidth > 0){
      return true;
    }
  }
  return false;
}

//wait for 10 seconds to load the page completely.
StartTimeOut=setTimeout(function(){start();},1000);
function start(){
  //check cookies button
  if ($('.qc-cmp-button').length>0){
    $('.qc-cmp-button')[0].click();
  }
  // check if we have Login button
  if ($('.ng-pristine .user a').length>0){
    chrome.runtime.sendMessage({greeting: 'AddUser'}, function(response) {});
    // WE ARE NOT LOGGED IN! Notify on the icon!
    $('.ng-pristine .user a')[0].click();
    chrome.runtime.sendMessage({greeting: 'DoLogin'}, function(response){
      if(lastID !== response.ID){
        tryLogin =0;
        lastID = response.ID;
      }else{
        tryLogin++;
      }
    });
    if(tryLogin<4){
      clearTimeout(StartTimeOut);
      StartTimeOut=setTimeout(function(){start();},5000);
    }
    // open shopping-bag
  }else if(calc_Done){
    didit=true;
  }else{
    check_status();
    if (trytwice<2){
      trytwice++;
      clearTimeout(StartTimeOut);
      StartTimeOut=setTimeout(function(){start();},5000);
    }else
      trytwice=0;
    //chrome.runtime.sendMessage({greeting: 'SetAtiveTab'}, function(response) {});
    //chrome.runtime.sendMessage({greeting: "setBadgeR=ERROR"}, function(response) {});
  }

  // Save last day it was OK
  if (didit){
    if(LoginBadge){
      var month = dateObj.getMonth() + 1; //months from 1-12
      var day = dateObj.getDate();
      chrome.runtime.sendMessage({greeting:'LoginBadge:'+dateObj}, function(response) {});
      chrome.storage.sync.set({SavedDay: day,SavedMonth: month}, function() {
        // saved, close the tab is we did it!
        trytwice=0;
        clearTimeout(SecondClickTimeOut);
        SecondClickTimeOut=setTimeout(function(){secondClick();},2000);
      });
    }else{
      get_login_badge();
      clearTimeout(StartTimeOut);
      StartTimeOut=setTimeout(function(){start();},5000);
    }
  }
  if(tryLogin>3){
    chrome.runtime.sendMessage({greeting: "setBadgeR=ERROR"}, function(response) {});
  }
}

function secondClick(){
  if($('.iziModal-content .icon-close').length>0 && $('.iziModal-content .icon-close').is(':visible')){
    document.getElementById("go-to-badges").click();
    $('.iziModal-content .icon-close')[0].click();
    if (trytwice<2){
      trytwice++;
      clearTimeout(SecondClickTimeOut);
      SecondClickTimeOut=setTimeout(function(){secondClick();},1500);
    }else{
      trytwice=0;
      clearTimeout(LogOutTimeOut);
      LogOutTimeOut=setTimeout(function(){logout();},100);
    }
  }else if ($('.container-fluid .card_smartvouchersbs>a').length>0){
    $('.container-fluid .card_smartvouchersbs>a')[0].click();
    clearTimeout(LogOutTimeOut);
    LogOutTimeOut=setTimeout(function(){logout();},5000);
  }else{
    if (trytwice<2){
      trytwice++;
      clearTimeout(SecondClickTimeOut);
      SecondClickTimeOut=setTimeout(function(){secondClick();},1500);
    }else{
      trytwice=0;
      clearTimeout(LogOutTimeOut);
      LogOutTimeOut=setTimeout(function(){logout();},100);
    }
  }
}

function logout(){
  if(is_S_W_L_open() === true){
    chrome.runtime.sendMessage({greeting: 'LogOut'}, function(response) {});
    $('.nav-link')[$('.nav-link').length-1].click();
  }else{
    $('.crc-img-sm.img-wrapper')[0].click();
    setTimeout(function(){logout();},1500);
  }
}

function check_status(){
  if($('.data').length>0){
    if(prefix === ''){
      keys = Object.keys(localStorage),
      i = keys.length;
      while ( i-- ) {
        if(keys[i].indexOf("wallet_income")>-1){
          prefix = keys[i].substring(0,keys[i].indexOf("wallet_income"));
          break;
        }
      }
    }
    if(JSON.parse(window.localStorage.getItem(prefix+'profile')) === null || JSON.parse(window.localStorage.getItem(prefix+'wallet_income')) === null){
      clearTimeout(CalcTimeOut);
      CalcTimeOut=setTimeout(function(){check_status();},1500);
      return;
    }else{
      var points_parse = parseInt(JSON.parse(window.localStorage.getItem(prefix+'wallet_income')).response.result.points_available);
      var points = points_parse/2500;
      var credits = parseFloat($('.data')[0].textContent.substring(2));
      var goal = 12.5;
      var total_credits = credits+points;
      while(goal <= total_credits){
        goal += 12.5;
      }
      var remaining_points = (goal-total_credits)*2500;
      var today_date_obj = new Date();
      var month_calc = true;
      var month_total = 0.0;
      var tomorrow_date_obj = new Date();
      tomorrow_date_obj.setDate(tomorrow_date_obj.getDate()+1);
      while(remaining_points > 0){
        today_date_obj.setDate(today_date_obj.getDate()+1);
        //If sunday
        if(today_date_obj.getDay()===0){
          remaining_points-=500;
        }
        //If last day of month
        tomorrow_date_obj.setDate(tomorrow_date_obj.getDate()+1);
        if(tomorrow_date_obj.getDate()===1){

          remaining_points-=1000;
          if(month_calc){
            month_total = remaining_points;
            month_calc = false;
          }
        }
        //Daily
        remaining_points-=100;
      }
      if(month_total === 0){
        month_total = goal;
      }else{
        month_total = goal-month_total/2500;
      }

      //    credits += points;
      var date = "No voucher found!";
      var last_bought = date;
      if(JSON.parse(window.localStorage.getItem(prefix+'profile')).data !== null){
        //date = JSON.parse(window.localStorage.getItem(prefix+'profile')).data.records[0].active_date;
        last_bought = JSON.parse(window.localStorage.getItem(prefix+'profile')).data.user_last_purchase_date;
      }

      var goal_date = ("00"+today_date_obj.getDate()).slice(-2)+"/"+("00"+(today_date_obj.getMonth()+1)).slice(-2);
      chrome.runtime.sendMessage({greeting:'AccountState:'+credits+'Points:'+points+'Date:'+date+'Goal:'+goal_date+'TMonth:'+month_total+'Last:'+last_bought}, function(response) {});
      calc_Done=true;
    }
  }else{
    if(is_S_W_L_open() === true){
      $('.nav-link')[1].click(); //open wallet
      clearTimeout(CalcTimeOut);
      CalcTimeOut=setTimeout(function(){check_status();},1500);
    }else{
      if($('.crc-img-sm.img-wrapper').length > 0){
        $('.crc-img-sm.img-wrapper')[0].click(); //Open profile
      }
      clearTimeout(CalcTimeOut);
      CalcTimeOut=setTimeout(function(){check_status();},1500);
    }
  }
}

function get_login_badge(){
  if(JSON.parse(window.localStorage.getItem(prefix+'user_badges')) !== null){
    var i = 0;
    while(JSON.parse(window.localStorage.getItem(prefix+'user_badges')).data[i].badge_name !== "daily_login"){
      i++;
    }
    var test_date = JSON.parse(window.localStorage.getItem(prefix+'user_badges')).data[i].earn_date;
    var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
    dateObj = new Date(test_date.replace(pattern,'$3-$2-$1'));
    LoginBadge = true;
  }else{
    if($('.nav-mybadges').length > 0){
      $('.nav-mybadges')[0].click();
      clearTimeout(BadgeTimeOut);
      BadgeTimeOut=setTimeout(function(){get_login_badge();},1500);
    }else{
      $('.nav-link')[0].click(); //open social
      clearTimeout(BadgeTimeOut);
      BadgeTimeOut=setTimeout(function(){get_login_badge();},1500);
    }
  }
}

