var CreditsCall = false;
var CreditsCallTimeOut = 0;
var CreditsState = [];
var PointsState = [];
var VoucherDate = [];
var GoalDate = [];
var TMonth = [];
var LBought = [];
var LLogin = [];

Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}

function restore_options() {
  // Use default value color = 'red' and likesColor = true.
  if(CreditsCall === false){
    chrome.runtime.sendMessage({greeting: 'GetCredits'}, function(response) {
      if(response !== undefined && response.CreditsState !== undefined){
        CreditsState = response.CreditsState;
        PointsState = response.PointsState;
        VoucherDate = response.VouchersDate;
        GoalDate = response.GoalDate;
        TMonth = response.TMonth;
        LBought = response.LBought;
        LLogin = response.LLogin;
        CreditsCall = true;
        restore_options();
      }
    });
  }else{
    accounts = JSON.parse(window.localStorage.getItem('SC_users'));
    if (typeof accounts !== 'undefined' && accounts !== null){
      var ul = document.getElementById("user_list");
      for(i=0;i<accounts.length; i++){
        var check="";
        if(LBought !== undefined && LBought !== null && LBought[i] !== undefined && LBought[i]!== null){
          var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
          var test_date = new Date(LBought[i].replace(pattern,'$3-$2-$1'));
          var now_date = new Date();
          if(now_date.getMonth()===test_date.getMonth()){
            check='&#10004;';
          }
        }
        var $newStudent;
        if(LLogin !== undefined && LLogin !== null && LLogin[i] !== undefined && LLogin[i] !== null){
          //test_date = new Date(LLogin[i].replace(pattern,'$3-$2-$1'));
          $newStudent = $("<tr><td title=\""+LLogin[i].substring(0,24)+"\">"+check+accounts[i].email+"</td>");
        }else{
          $newStudent = $("<tr><td>"+check+accounts[i].email+"</td>");
        }
        if(CreditsState[i] !== undefined && CreditsState[i] !== null && CreditsState[i] >= 0){
          if(PointsState[i] !== undefined && PointsState[i] !== null && PointsState[i] >= 0){
            $newStudent.append("<td title=\""+TMonth[i]+"\">"+(CreditsState[i]+PointsState[i]).toFixed(2)+
                               " &#8364 (" + CreditsState[i].toFixed(2)+" + "+PointsState[i].toFixed(2)+")</td>");
          }else{
            $newStudent.append("<td title=\""+TMonth[i]+"\">"+CreditsState[i].toFixed(2)+" &#8364</td>");
          }
        }else{
          $newStudent.append("<td>Read error.</td>");
        }

        if(VoucherDate[i] !== undefined && VoucherDate[i]!== null){
          $newStudent.append("<td title=\""+GoalDate[i]+"\">"+VoucherDate[i].substring(0,10)+"</td>");
        }else{
          $newStudent.append("<td>Read error.</td>");
        }
        $newStudent.append("<td><input id=checkUpdate name=\"account"+i+"\" type=\"checkbox\"></td>");
        $newStudent.append("</tr>");
        $('#user_list').append($newStudent);
        var cells = document.getElementsByTagName('td');
        if(CreditsState[i] !== undefined){
          if(CreditsState[i]>=12.5){
            cells[i*4+1].style.backgroundColor = 'orange';
          }
        }else{
          cells[i*4+1].style.backgroundColor = 'grey';
        }

        if(VoucherDate[i] !== undefined && VoucherDate[i]!== null){
          test_date = new Date(VoucherDate[i].replace(pattern,'$3-$2-$1'));
          now_date = new Date();
          if(now_date>=test_date){
            var month_diff = (now_date.getMonth()-test_date.getMonth())*75;
            cells[i*4+2].style.backgroundColor = "rgb(255, " + String(255-month_diff) + ", "+String(month_diff)+")";
          }
        }else{
          cells[i*4+2].style.backgroundColor = 'grey';
        }
      }
    }
    var update_button = document.getElementById("button");
    if(update_button !== null)
      update_button.addEventListener('click', update_account);
  }
}

function update_account(element){
  var UpdateState = "";
  if (typeof accounts !== 'undefined' && accounts !== null){
    for(i=0;i<accounts.length; i++){
      if(document.getElementsByName("account"+i)[0].checked){
        UpdateState += ","+i;
      }
    }
    chrome.runtime.sendMessage({greeting: 'UpdateLogin:'+UpdateState.substring(1)}, function(response){});
  }
}

var accounts = [];
document.addEventListener('DOMContentLoaded', restore_options);
