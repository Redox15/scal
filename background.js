var badgeText="init";
var myTAB;
if (myTAB === undefined)
  myTAB=0;
var scriptLastCall = 0;
if (scriptLastCall === undefined)
  scriptLastCall=0;
var dateObj = new Date();
var month = 0;//dateObj.getMonth() + 1; //months from 1-12
var day = 0;//dateObj.getDate();
var hours = 1;//dateObj.getHours();
var accountID = 0;
var time_out;
var accounts_object = window.localStorage.getItem('SC_users');
var accounts = JSON.parse(accounts_object);

var login_state = Array(accounts.length).fill(0);
var credits_state = Array(accounts.length).fill(0.0);
var points_state = Array(accounts.length).fill(0.0);
var voucher_date_state = Array(accounts.length).fill("");
var goal_date_state = Array(accounts.length).fill("");
var total_month_state = Array(accounts.length).fill(0.0);
var last_bought_state = Array(accounts.length).fill("");
var login_badge_state = Array(accounts.length).fill("");

var userID;
var credential;
var firebaseID;
var license_status = false;
var license_state = "FREE_TRIAL";
var license_date = dateObj.getTime();
var TRIAL_PERIOD_DAYS = 7;

chrome.browserAction.setBadgeText({text: badgeText});

function openTab(link){
  chrome.tabs.query({}, function(tabs) {
    for(i=0;i<tabs.length;i++){
      if(tabs[i].url.indexOf(link)>-1){
        myTAB = tabs[i].id;
        break;
      }
    }
    if(i === tabs.length){
      chrome.tabs.create({'url': link, active: false}, function(tab) {myTAB=tab.id;});
    }
  });
}

function getSXC_ID(){
  chrome.tabs.query({title: "SixthContinent"}, function(tabs) {
    if(tabs.length === 0){
      myTAB = 0;
    }else{
      myTAB = tabs[0].id;
    }
  });
}

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDWumk7Wp5h4BN02SILiHnOr0vSP3WE_VY",
  authDomain: "sxcvouchers.firebaseapp.com",
  databaseURL: "https://sxcvouchers.firebaseio.com",
  projectId: "sxcvouchers",
  storageBucket: "sxcvouchers.appspot.com",
  messagingSenderId: "199374186621",
  appId: "1:199374186621:web:e57700898f40b86d"
};
const app = firebase.initializeApp(config);
const appDb = app.database();

firebase.auth().onAuthStateChanged(function(user) {
  //alert('User state change detected from the Background script of the Chrome Extension: '+user.email);
  if(user !== null){
    firebaseID=user.uid;
    read_license();
  }
});

function read_license(){
  appDb.ref('users/' + firebaseID).once('value').then(function(snapshot) {
    if(snapshot.val() === null){
      appDb.ref('users/'+firebaseID).set({date: license_date,
                                           license: license_state
                                         });
    }else{
      license_date = snapshot.val().date;
      license_state = snapshot.val().license;
    }
    clearTimeout(time_out);
    time_out = setTimeout(function(){check_license();},1000);
  });
}

chrome.identity.getAuthToken({ 'interactive': true }, function(token) {
  accessToken = token;
  credential = firebase.auth.GoogleAuthProvider.credential(null, token);
  firebase.auth().signInWithCredential(credential).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    if (errorCode === 'auth/account-exists-with-different-credential') {
      console.log('Email already associated with another account.');
      // Handle account linking here, if using.
    } else {
      console.log(error);
    }
  });
});


// run script always
chrome.browserAction.onClicked.addListener(function (tab) { //Fired when User Clicks ICON
  accountID = 0;
  accounts_object = window.localStorage.getItem('SC_users');
  accounts = JSON.parse(accounts_object);
  chrome.browserAction.getBadgeText({}, function(result) {
    badgeText=result;
    //alert('This extension is always active, it will open SixthContinent website once everyday, click on SHOP icon and then automatically close it!');
    // clear the text on icon
    getSXC_ID();
    if(myTAB > 0){
      chrome.tabs.update(myTAB, {selected: true});
    }
    chrome.browserAction.setBadgeText({text: ''});
    startTimerIfNeed();
    //createNewPageAndRun('https://www.sixthcontinent.com/');
  });
});

chrome.runtime.onMessage.addListener(function(message,sender,sendResponse){
  if (message.greeting.indexOf('ww.')>-1){
    chrome.tabs.query({}, function(tabs) {
      if(tabs.length === 0){
        chrome.tabs.create({'url': message.greeting, active: false}, function(tab) {myTAB=tab.id;});
      }else{
        for(i=0;i<tabs.length;i++){
          if(tabs[i].url.indexOf(message.greeting.substring(4))>-1){
            break;
          }
        }
        if(i === tabs.length){
          chrome.tabs.create({'url': message.greeting, active: false}, function(tab) {myTAB=tab.id;});
        }
      }
    });
  }else if (message.greeting.indexOf('LogOut')>-1 && myTAB>0){
    chrome.browserAction.setBadgeText({text: ''});
    clearTimeout(time_out);
    time_out = setTimeout(function(){
      startTimerIfNeed();
    },5000);
  }else if (message.greeting.indexOf('CLOSETHIS')>-1 && myTAB>0){
    chrome.tabs.remove(myTAB);
    chrome.browserAction.setBadgeText({text: ""});
  }else if (message.greeting.indexOf('SetAtiveTab')>-1 && myTAB>0){
    chrome.tabs.update(myTAB, {selected: true});
  }else if (message.greeting.indexOf("setBadgeR=")>-1){
    chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
    chrome.browserAction.setBadgeText({text: message.greeting.substring(10)});
  }else if (message.greeting.indexOf("setBadgeG=")>-1){
    chrome.browserAction.setBadgeBackgroundColor({ color: [0, 255, 0, 255] });
    chrome.browserAction.setBadgeText({text: message.greeting.substring(10)});
  }else if (message.greeting.indexOf("UpdateLogin")>-1){
    var update_list = message.greeting.substring(12).split(",");
    update_list.forEach(function(account){
      login_state[account].day=0;
      login_state[account].month=-1;
    });

    chrome.browserAction.setBadgeBackgroundColor({ color: [102, 204, 255, 255] });
    chrome.storage.sync.set({SavedState: login_state},function(){});
    chrome.storage.sync.set({SavedDay: login_state[0].day,SavedMonth: login_state[0].month}, function() {});
    clearTimeout(time_out);
    time_out = setTimeout(function(){read_license()},100);
  }else if (message.greeting.indexOf("DoLogin")>-1){
    var login_message = {
      command: 'ngi-toggle-'+accountID
    };
    sendResponse({ID:accountID});
    if(accountID < accounts.length){
      chrome.tabs.query({title: "SixthContinent"}, function(tabs) {
        chrome.tabs.sendMessage(tabs[0].id, login_message);
      });
    }
  }else if (message.greeting.indexOf("AddUser")>-1){
    accounts_object = window.localStorage.getItem('SC_users');
    accounts = JSON.parse(accounts_object);
    var user_message = {
      command: accounts_object
    };
    chrome.tabs.query({title: "SixthContinent"}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, user_message);
    });
  }else if (message.greeting.indexOf("AccountState")>-1){
    var credits = parseFloat(message.greeting.substring(13,message.greeting.indexOf("Points")));
    var points = parseFloat(message.greeting.substring(message.greeting.indexOf("Points")+7,message.greeting.indexOf("Date")));
    var voucher_date = message.greeting.substring(message.greeting.indexOf("Date")+5,message.greeting.indexOf("Goal"));
    var goal_date = message.greeting.substring(message.greeting.indexOf("Goal")+5,message.greeting.indexOf("TMonth"));
    var total_month = parseFloat(message.greeting.substring(message.greeting.indexOf("TMonth")+7,message.greeting.indexOf("Last:")));
    var last_bought = message.greeting.substring(message.greeting.indexOf("Last:")+5);
    var test = 0;

    credits_state[accountID] = credits;
    points_state[accountID] = points;
    voucher_date_state[accountID] = voucher_date;
    goal_date_state[accountID] = goal_date;
    total_month_state[accountID] = total_month;
    last_bought_state[accountID] = last_bought;

    chrome.storage.sync.set({
                              SC_users_credits: credits_state,
                              SC_users_points: points_state,
                              SC_users_dates: voucher_date_state,
                              SC_users_goals: goal_date_state,
                              SC_users_tmonth: total_month_state,
                              SC_users_lbought: last_bought_state
                            }, function() {});
  }else if (message.greeting.indexOf("GetCredits")>-1){
    chrome.storage.sync.get({
                              SC_users_goals: [],
                              SC_users_dates: [],
                              SC_users_tmonth: [],
                              SC_users_credits: [],
                              SC_users_points: [],
                              SC_users_lbought: [],
                              SC_users_LoginBadge: []
                            },function(items){
                              try {
                                if (chrome.runtime.lastError) {
                                  console.warn(chrome.runtime.lastError.message);
                                  console.warn("GetCredits runtime Error");
                                } else {
                                  var resp_credits_state = [];
                                  var resp_points_state = [];
                                  var resp_voucher_date_state = [];
                                  var resp_goal_date_state = items.SC_users_goals;
                                  var resp_total_month_state = items.SC_users_tmonth;
                                  var resp_last_bought_state = items.SC_users_lbought;
                                  credits_state = items.SC_users_credits;
                                  points_state = items.SC_users_points;
                                  voucher_date_state = items.SC_users_dates;
                                  goal_date_state = items.SC_users_goals;
                                  total_month_state = items.SC_users_tmonth;
                                  last_bought_state = items.SC_users_lbought;

                                  if(JSON.parse(window.localStorage.getItem('calc_discount'))){
                                    resp_credits_state = items.SC_users_credits;
                                    resp_points_state = items.SC_users_points;
                                  }
                                  if(JSON.parse(window.localStorage.getItem('check_download'))){
                                    resp_voucher_date_state = items.SC_users_dates;
                                  }
                                  var resp_last_login = items.SC_users_LoginBadge;
                                  sendResponse({CreditsState:resp_credits_state,PointsState:resp_points_state,VouchersDate:resp_voucher_date_state,GoalDate:resp_goal_date_state,TMonth:resp_total_month_state,LBought:resp_last_bought_state,LLogin:resp_last_login});
                                }
                              }catch (exception) {
                                window.alert('exception.stack: ' + exception.stack);
                                console.error((new Date()).toJSON(), "exception.stack:", exception.stack);
                              }
                            });
  }else if (message.greeting.indexOf("LoginBadge")>-1){
    var login_badge = message.greeting.substring(11);
    login_badge_state[accountID] = login_badge;
    chrome.storage.sync.set({SC_users_LoginBadge: login_badge_state}, function() {});
  }
  return true;
});

function check_license(){
  console.log(license_date);
  if(license_state === "FULL"){
    console.log("Fully paid & properly licensed.");
    license_status = true;
  }else if(license_state === "FREE_TRIAL"){
    var daysAgoLicenseIssued = Date.now() - license_date;
    daysAgoLicenseIssued = daysAgoLicenseIssued/1000/60/60/24;
    if(daysAgoLicenseIssued <= TRIAL_PERIOD_DAYS) {
      console.log("Free trial, still within trial period");
      license_status = true;
    }else{
      console.log("Free trial, trial period expired.");
      license_status = false;
    }
  }else{
    console.log("No license ever issued.");
    license_status = false;
  }
  chrome.storage.sync.get({
                            SC_users_goals: [],
                            SC_users_dates: [],
                            SC_users_tmonth: [],
                            SC_users_credits: [],
                            SC_users_points: [],
                            SC_users_lbought: [],
                            SC_users_LoginBadge: []
                          },function(items){
                            try {
                              if (chrome.runtime.lastError) {
                                console.warn(chrome.runtime.lastError.message);
                                console.warn("GetCredits runtime Error");
                              } else {
                                credits_state = items.SC_users_credits;
                                points_state = items.SC_users_points;
                                voucher_date_state = items.SC_users_dates;
                                goal_date_state = items.SC_users_goals;
                                total_month_state = items.SC_users_tmonth;
                                last_bought_state = items.SC_users_lbought;
                                login_badge_state = items.SC_users_LoginBadge;
                              }
                            }catch (exception) {
                              window.alert('exception.stack: ' + exception.stack);
                              console.error((new Date()).toJSON(), "exception.stack:", exception.stack);
                            }
                          });
  //start timer every second
  if(!license_status){
    chrome.browserAction.setBadgeText({text: "Expired"});
    chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
  }else{
    clearTimeout(time_out);
    time_out = setTimeout(function(){startTimerIfNeed();},1000);
  }
}

// timer every hour
function startTimerIfNeed(){
  // read variable when we last time open the page
  if (parseInt(Math.floor(Date.now()))>scriptLastCall){
    scriptLastCall = parseInt(Math.floor(Date.now()));
    chrome.browserAction.getBadgeText({}, function(result) {
      chrome.storage.sync.get({
                                SavedDay:0,
                                SavedMonth:0,
                                SavedState:[]
                              }, function(items) {
                                if(items.SavedState[accountID] !== undefined){
                                  login_state = items.SavedState;
                                }
                                if(login_state[accountID] === undefined){
                                  login_state.push({"day":0,"month":-1});
                                }else{
                                  login_state[accountID].day = items.SavedDay;
                                  login_state[accountID].month = items.SavedMonth;
                                }
                                dateObj = new Date();
                                month = dateObj.getMonth() + 1; //months from 1-12
                                day = dateObj.getDate();
                                hours = dateObj.getHours();
                                if (login_state[accountID].day!==day || login_state[accountID].month!==month){
                                  if (hours!==0){
                                    chrome.browserAction.setBadgeText({text: 'Run'});
                                    chrome.browserAction.setBadgeBackgroundColor({ color: [0, 0, 255, 255] });
                                    createNewPageAndRun('https://www.sixthcontinent.com/');
                                    clearTimeout(time_out);
                                    time_out = setTimeout(function(){startTimerIfNeed();},60000);
                                  }else{
                                    // we have to check but it's 00.xx now!
                                    chrome.browserAction.setBadgeText({text: '00.xx'});
                                    dateObj.setHours(1);
                                    dateObj.setMinutes(0);
                                    clearTimeout(time_out);
                                    time_out = setTimeout(function(){startTimerIfNeed();},dateObj-parseInt(Math.floor(Date.now())));
                                  }
                                }else if (login_state[accountID].day===day && login_state[accountID].month===month && hours!==0){//&& result===""
                                  accountID++;
                                  if(accountID < accounts.length){
                                    if(login_state[accountID] === undefined){
                                      chrome.storage.sync.set({SavedDay: 0,SavedMonth: -1}, function() {});
                                    }else{
                                      chrome.storage.sync.set({SavedDay: login_state[accountID].day,SavedMonth: login_state[accountID].month}, function() {});
                                    }
                                    clearTimeout(time_out);
                                    time_out = setTimeout(function(){startTimerIfNeed();},2000);
                                  }else{
                                    if(myTAB>0){
                                      setTimeout(function(){
                                        chrome.tabs.remove(myTAB);
                                      },5000);
                                    }
                                    chrome.browserAction.setBadgeBackgroundColor({ color: [0, 255, 0, 255] });
                                    chrome.browserAction.setBadgeText({text: 'OK'});
                                    dateObj.setDate(day+1);
                                    dateObj.setHours(1);
                                    dateObj.setMinutes(0);
                                    var timeLeftTo1AM = dateObj-parseInt(Math.floor(Date.now()));
                                    clearTimeout(time_out);
                                    time_out = setTimeout(function(){read_license();},timeLeftTo1AM);
                                    accountID = 0;
                                  }
                                  chrome.storage.sync.set({SavedState: login_state},function(){});
                                }
                              });
    });
  }
}

chrome.tabs.onRemoved.addListener(function(tabId, removeInfo){
  getSXC_ID();
});

function createNewPageAndRun(_link){
  chrome.browserAction.getBadgeText({}, function(result) {
    badgeText=result;
    getSXC_ID();
    if(myTAB === 0){
      openTab(_link);
      setTimeout(function(){createNewPageAndRun(_link);},5000);
    }else{
      chrome.tabs.executeScript(myTAB, { file: "angular.js" }, function() {
        chrome.tabs.executeScript(myTAB, { file: "jquery-3.3.1.min.js" }, function() {
          chrome.tabs.executeScript(myTAB, { file: "sixthcontinent.js" });
        });
      });
    }
  });
}

if(chrome.contextMenus){
  chrome.contextMenus.create({title: "Open SixthContinent", contexts: ["browser_action"], onclick:function() {
    getSXC_ID();
    if(myTAB > 0){
      chrome.tabs.update(myTAB, {selected: true});
    }else{
      chrome.tabs.create({'url': 'https://www.sixthcontinent.com/', active: true}, function(tab) {myTAB=tab.id;});
    }
  }});
  chrome.contextMenus.create({title: "Repeat Login", contexts: ["browser_action"], onclick:function() {
    var reset_login_state = [];
    login_state = [];
    chrome.storage.sync.set({SavedDay: 0,SavedMonth: -1}, function() {});
    chrome.storage.sync.set({SavedState: reset_login_state},function(){});
    window.localStorage.setItem('SC_users_credits', []);
    credits_state =[];
    points_state =[];
    window.localStorage.setItem('SC_users_dates', []);
    voucher_date_state=[];
    clearTimeout(time_out);
    time_out = setTimeout(function(){read_license()},100);
  }});
}
